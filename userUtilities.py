from flask import Flask
from databaseOperations import searchUserByUserId
from passlib.hash import sha256_crypt


app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('databaseConfig.py')


# Check if the userAgent has previleges enough
def allowIntendedAction(dbUserProfile, intendedAction):

    if(intendedAction == app.config['WRITE']):
        return (dbUserProfile == app.config['READ_WRITE'] or dbUserProfile == app.config['WRITE'])

    if(intendedAction == app.config['READ']):
        return (dbUserProfile == app.config['READ_WRITE'] or dbUserProfile == app.config['READ'])

    return False


#  Validates the credencials (user, password) of the user who is executing the methods
def validateWebAgentCredentials(request):

    auth = request.authorization

    if not hasattr(auth, "username"):
        # return jsonify({'error':'Access denied, not username provided'}), 401
        return "error", 'Access denied, not username provided', 401, None
    if not hasattr(auth, "password"):
        # return jsonify({'error':'Acces denied, not password provided'}), 401
        return "error", 'Access denied, not password provided', 401, None

    usersDB = searchUserByUserId(auth.username)

    if not usersDB:
        # return  jsonify({'Error':'Access denied, not USERID found in DB!!!'}), 401
        return "error", 'Access denied, not USERID found in DB!!!', 401, None

    if not validateUserPassword(auth.password, usersDB[0].password):
        # return  jsonify({'Error':'User sending request fails login password!!!'}), 401
        return "error", 'User sending request fails login password!!!', 401, None

    return "","",500,usersDB[0]


# It compares userAgentPassword(it is on the clear) with the hashed password got from DB
def validateUserPassword(userAgentPassword, dbHashedUserPassword):
    return sha256_crypt.verify(userAgentPassword, dbHashedUserPassword);
    # return True