FROM python:3.7-alpine
WORKDIR /miniproject
COPY . /miniproject
RUN pip install -U -r requirements.txt
EXPOSE 8080
CMD ["python3", "mini.py"]