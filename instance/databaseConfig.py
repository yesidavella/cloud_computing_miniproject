SERVER_URL = "cassandra"

# Rolles allowed in users table
READ = "read"
WRITE = "write"
READ_WRITE = "read_write"
