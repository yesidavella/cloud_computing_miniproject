# cloud_computing_miniproject

**ECS781P: Cloud Computing**
**Author: Yesid Andres Avella Ospina**
**Student ID:	170941893/1**
**Mail: y.a.avellaospina@se18.qmul.ac.uk**

Miniproject GitLab URL: https://gitlab.com/yesidavella/cloud_computing_miniproject
Clone URL: https://gitlab.com/yesidavella/cloud_computing_miniproject.git

**INTRODUCTION**

This python application brings a restAPI to a big database of pets lost in United States. In addition, it also brings user administration to control the access and modification of users from web Agents.  

The external rest API is https://www.petfinder.com/developers/ 

**Basic flow of communication: **

To test if the miniproject is running just type this in any browser: 35.246.53.58/ it is not needed any type of authentication so far!!! 

If your answer is <h1>Helloo!! miniproject working, cheers from Yesid!!!</h1>  

The connection can be triggered from a rest client, for example postman, the web agent user (in this case the person who uses postman) should use “Basic Auth” (user: admin and password:admin123) and would be capable of using the next methods: 

**To test if miniproject is running:**
35.246.53.58/ 

**To query in petFinder.com the dogs in pages of 10 **

35.246.53.58/animals 

And to find, lets say cats in groups of 3 

35.246.53.58/animals?amount=3&type=cat 

**If you wish to find pets by its identification, you can use: **

35.246.53.58:80/animal/{id} lets say 35.246.53.58:80/animal/44333581 


**User administration: **

You can create users or add them, just remember that you should be always logged
with user and password with privileges enough. For instance admin admin123 


**To set a new user the address is: **

35.246.53.58:80/config/newuser 

Just remember that you should do it using and a user with write previlegies e.g. us:admin and pass:admin123 it goes along with a JSON body. 

{ 
    "id": "reader1", 
    "password": "reader1123", 
    "profile": "read"
}

To erase user the URL is 35.246.53.58:80/config/deleteuser/{userId} let say 35.246.53.58:80/config/deleteuser/reader1 


**ARCHITECTURE** 

This project is compounded by the miniproject, the petFinder.com restApi and a cassandra data base. 

An external agent should interact through a GET, POST or DELETE method with miniproject, depending of the URL invoked, it could get results from pets in a variety of modalities as well as manage users to control the interaction. 

As all the interaction coming from the user agent (let's say postman), should be using a login with password, which is stored hashed in the database.  

The bypass process between miniproject artifact with petFinder.com restApi is made asking for a token which is cached during one hour, after it, miniproject ask for another. Basically, miniproject is registered with petFinder Api. 

**The table user of the data base looks in the next way:** 

id  | password| profile  

Where all are text fields.  

The roles of the users should be:
read, write and read_write

JUST USE THIS CODE IN THE CASSANDRA CONSOLE TO RECREATE THE BASIC DB:

To create a keyspace:
CREATE KEYSPACE miniproject WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor' : 2};

To create the user table:
CREATE TABLE miniproject.USER (id text PRIMARY KEY,password text, profile text);

As it checks the rol of the user,the admin user should be inserted by db:
INSERT INTO miniproject.user (id, password, profile) VALUES ('admin','$5$rounds=535000$Xe9VG5I/3ohEXAgH$XuZwmS15pg7x4kFz59lzjCncUMxKCMzBDsHv6xslJr5', 'read_write');
The previous admin user has password: admin123

Query the users:
select * from miniproject.user ALLOW FILTERING; 

When admin user created, from postman you can create more users like the next one:
{
    "id": "writer",
    "password": "writer123",
    "profile": "write"
}
 

**EXTRA INFORMATION**

* These are the credential used by miniproject to connect and to be authenticated with petFinder.com rest Api.

API Key
W6s0417zccOhkQ2RG8aXm8PgSv6l5pxeV8FdAsv2kCkySShfUA 

Secret
DvFkeYVQ9hXi51hRnclf2Lwn76yMSNFksDOcJEOY

* The project was provided with requirements.txt and Dockerfile if any dependence is requiered runing the code.
