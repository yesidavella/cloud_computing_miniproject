from flask import Flask, request, jsonify
from cachetools import cached, TTLCache
from userUtilities import allowIntendedAction, validateWebAgentCredentials
from databaseOperations import deleteUserDBById, insertUser
import requests

# Cached object which is going to store ONLY 1 object lasting 1hour in memory
petfinderCacheObject = TTLCache(maxsize=1, ttl=3600)


app = Flask(__name__, instance_relative_config=True)
# Loading params from external files
app.config.from_pyfile('tokenLogin.py')
app.config.from_pyfile('databaseConfig.py')


# This is the test method to check if the app is running
@app.route('/')
def hello():
    return('<h1>Helloo!! miniproject working, cheers from Yesid!!!</h1>')


# Cached method just responsible of request a token to be able to make request in www.petfinder.com rest api
@cached(petfinderCacheObject)
def getTokenFromPetFinder():

    print("Beginning call getTokenFromPetFinder()... ")
    clientId = app.config['CLIENT_ID']
    clientSecret = app.config['CLIENT_SECRET']

    payload = {'grant_type':'client_credentials', 'client_id':clientId, 'client_secret':clientSecret}

    petfinderTokenResp = requests.post("https://api.petfinder.com/v2/oauth2/token", data=payload)

    if petfinderTokenResp.ok:
        print("Success: petfinderTokenResp.ok!!! New token on the way!!!")
        tokenData = petfinderTokenResp.json()
        petfinderToken = tokenData.get('access_token')
    else:
        print("Reason to fail: " + petfinderTokenResp.reason)

    print("Finishing call getTokenFromPetFinder()")
    return petfinderToken


# This method shows all the animals as a json answer, receives as optional the params type and
# amount. e.g. /animals?type=cat&amount=4. By default it is type=dog&amount=10
@app.route('/animals', methods=['GET'])
def animals():

    print("Begining call animals()... ")

    credentialAns = validateWebAgentCredentials(request)

    if not credentialAns[2]==500:
        return  jsonify({str(credentialAns[0]):str(credentialAns[1])}), credentialAns[2]

    usersDB = credentialAns[3]

    dbUserId = usersDB.id
    dbUserPass = usersDB.password
    dbUserProfile = usersDB.profile

    if allowIntendedAction(dbUserProfile, app.config['READ']):

        petfinderToken = getTokenFromPetFinder()

        animalType = request.args.get('type', default = 'dog', type = str)
        amount = request.args.get('amount', default = '10', type = str)

        resp = requests.get('https://api.petfinder.com/v2/animals?type='+animalType+'&limit='+amount, headers={'Authorization': 'Bearer '+petfinderToken})
        return handleResponseInJSONFormat(resp)
    else:
        return  jsonify({'Error':'Failure finding animals, you are not entitled to find '}), 401


# This method receives animal ID as placeholder. e.g. /animals/{animalID}
@app.route('/animal/<animalId>', methods=['GET'])
def getAnimalById(animalId):

    print("Begining call getAnimalById()... ")

    credentialAns = validateWebAgentCredentials(request)

    if not credentialAns[2]==500:
        return  jsonify({str(credentialAns[0]):str(credentialAns[1])}), credentialAns[2]

    usersDB = credentialAns[3]

    dbUserId = usersDB.id
    dbUserPass = usersDB.password
    dbUserProfile = usersDB.profile

    if allowIntendedAction(dbUserProfile, app.config['READ']):
        petfinderToken = getTokenFromPetFinder()
        resp = requests.get('https://api.petfinder.com/v2/animals/'+animalId, headers={'Authorization': 'Bearer '+petfinderToken})
        return handleResponseInJSONFormat(resp)
    else:
        return  jsonify({'Error':'Failure finding animal by ID, you are not entitled to find'}), 401


# This method receives the required params to create a user.
@app.route('/config/newuser', methods=['POST'])
def setNewUser():

    print("Begining call setNewUser()... ")

    credentialAnswer = validateWebAgentCredentials(request)

    if not credentialAnswer[2]==500:
        return  jsonify({str(credentialAnswer[0]):str(credentialAnswer[1])}), credentialAnswer[2]

    usersDB = credentialAnswer[3]

    dbUserId = usersDB.id
    dbUserPass = usersDB.password
    dbUserProfile = usersDB.profile

    if allowIntendedAction(dbUserProfile, app.config['WRITE']):

        if not request.json or not 'id' in request.json or not 'password' in request.json or not 'profile' in request.json:
            return jsonify({'error':'The JSON info is not compliant'}), 201

        dbAnswer = insertUser(request.json['id'], request.json['password'], request.json['profile'])

        return  jsonify({dbAnswer[0]:dbAnswer[1]}), dbAnswer[2]
    else:
        return  jsonify({'Error':'Failure inserting, you are not entitled to insert'}), 401


# This method receives animal ID as placeholder. e.g. /animals/{animalID}
@app.route('/config/deleteuser/<userId>', methods=['DELETE'])
def deleteUserById(userId):

    print("Beginning call deleteUserById()... ")

    credentialAnswer = validateWebAgentCredentials(request)

    if not credentialAnswer[2]==500:
        return  jsonify({str(credentialAnswer[0]):str(credentialAnswer[1])}), credentialAnswer[2]

    usersDB = credentialAnswer[3]

    dbUserId = usersDB.id
    dbUserPass = usersDB.password
    dbUserProfile = usersDB.profile

    if allowIntendedAction(dbUserProfile, app.config['WRITE']):
        dbAns = deleteUserDBById(userId)
        return  jsonify({dbAns[0]:dbAns[1]}), dbAns[2]
    else:
        return  jsonify({'Error':'Failure DELETING, you are not entitled to DELETE'}), 401


# This method is in charge of managing the success or failure code, just in reference to petfinder.com asnwers
def handleResponseInJSONFormat(respFromPethFinder):

    if respFromPethFinder.ok:
        jsonResponse = app.response_class(
            response=respFromPethFinder.content,
            status=200,
            mimetype='application/json'
        )
    elif respFromPethFinder.status_code == 404:
        return jsonify({'error':'Pet not found!'}), 404
    else:
        print("Failure reason: " + respFromPethFinder.reason + ". With code: " + str(respFromPethFinder.status_code))
        return jsonify({'error':'Error calling petFinder!'}), 500
    return jsonResponse



###################init method#######################
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
