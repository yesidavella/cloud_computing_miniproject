from flask import Flask
from passlib.hash import sha256_crypt
from cassandra.cluster import Cluster

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('databaseConfig.py')

# This has to be the name of the cassandra db. Check it on /instance/databaseConfig.py
cluster = Cluster([app.config['SERVER_URL']])
session = cluster.connect()


# This method inserts one user in the table users, hashing the user password
def insertUser(userId, userPassword, userProfile):
    session.execute("""INSERT INTO miniproject.USER (id, password, profile) VALUES (%s, %s, %s)""",
                    (userId, sha256_crypt.encrypt(userPassword), userProfile))
    return 'success','User inserted successfully!!! Well done!!!', 201


# This method finds one user in the table users by userId
def searchUserByUserId(userId):
    return session.execute( """Select * From miniproject.user where id = '{}'""".format(userId))


# This method deletes one user in the table users by userId
def deleteUserDBById(userId):
    session.execute("""DELETE FROM miniproject.USER where id = '{}'""".format(userId))
    return 'success','User {} deleted successfully!!! Well done!!!'.format(userId), 200
